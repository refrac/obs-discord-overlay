const config = require("./config.json");
const fetch = require("node-fetch");

var lastMessage;

const SocketServer = require('ws').Server;
const express = require("express");
var app = express();
var path = require('path');

var nickList = {};
var currentColor = 0;

app.use('/fonts', express.static('fonts'));

const port = process.env.PORT || 1314;
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
});
var server = app.listen(port, function () {
  console.log('node.js static server listening on port: ' + port)
});

const wss = new SocketServer({ server });

//init Websocket ws and handle incoming connect requests
wss.on('connection', function connection(ws) {
  console.log("connection ...");
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
  });
});

function query(endpoint) {
  return fetch('https://discord.com/api'+endpoint, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': config.token
    }
  })
  .then(r => r.json());
}

function getMessages(lastMessage) {
  return query('/channels/'+config.channel+'/messages?after='+lastMessage);
}

function reloadMessages() {
  // console.log(lastMessage);
  getMessages(lastMessage)
  .then(data => {
    if (data.length > 0) {
      data.sort((a, b) => {
        return new Date(a.timestamp) - new Date(b.timestamp);
      });
      // console.log(data);
      data.forEach(message => formatMessage(message));
    }
    // console.log(data);
  });
}

function nextColor() {
  if (currentColor > 7) {
    currentColor = 1;
    // console.log(currentColor + ' reset');
  } else {
    currentColor++;
    // console.log(currentColor + ' increment');
  }
}

function findNick(message) {
  // console.log(message);
  var userid = message.author.id;
  var username = message.author.username;
  if (message.author.bot === true) {
    if (nickList[username] === undefined) {
      return new Promise((resolve, reject) => {
        // console.log('-- bot');
        nextColor();
        nickList[username] = {};
        nickList[username].name = username;
        nickList[username].color = currentColor;
        // console.log('-- not found');
        resolve();
      });
    } else {
      // console.log('-- found');
      return new Promise((resolve, reject) => { resolve(); });
    }
  } else {
    if (nickList[userid] === undefined) {
      return new Promise((resolve, reject) => {
        query('/guilds/'+config.guild+'/members/'+userid)
        .then(data => {
          nextColor();
          // console.log(nickList);
          nickList[userid] = {};
          nickList[userid].name = (data.nick === null) ? data.user.username : data.nick;
          nickList[userid].color = currentColor;
          resolve();
        });
      });
    } else {
      return new Promise((resolve, reject) => { resolve(); });
    }
  }
}

function findContentNick(content) {
  // console.log('-- findContentNick: ' + content);
  reg = /<@!([0-9]*)>/g;
  if (content.match(reg)) {
    matches = content.matchAll(reg);
    prom = [];
    for (const match of matches) {
      prom.push(findNick({ author: { id: match[1], username: 'xxx', bot: false } }));
    }
    return Promise.all(prom);
  } else {
    return new Promise((resolve, reject) => { resolve(); });
  }
}

function formatMessage(message) {
  findNick(message)
  .then(findContentNick(message.content))
  .then(() => {
    console.log(nickList);
    console.log(message);
    let payload = {
      "user": (message.author.bot === true) ? nickList[message.author.username] : nickList[message.author.id],
      "content": message.content
        .replace(/\n+/g, '<br>')
        .replace(/<(:[^:]*:)[0-9]*>/g, '$1')
        .replace(/<@!([0-9]*)>/g, (_, match) => {
          return '<b>@'+nickList[match].name+'</b>';
        }),
      "timestamp": new Date(message.timestamp)
        .toLocaleTimeString("fr-FR", {
          timezone: "Europe/Paris",
          hour: "2-digit",
          minute: "2-digit"
        })
    }
    lastMessage = message.id;
    wss.clients.forEach((client) => {
      if (client.readyState === 1) {
        client.send(JSON.stringify(payload));
      }
    });
    console.log(Object.keys(nickList).length + ' users cached');
    console.log(payload);
  })
  .catch(err => {
    console.log("============== Error ==============");
    console.log(err);
    lastMessage = message.id;
  })
}

query('/channels/'+config.channel)
.then(data => {
  lastMessage = data.last_message_id;
  setInterval(reloadMessages, 3000);
});
