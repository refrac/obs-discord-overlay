OBS Discord Overlay
=======================

This is just a replacement script for having more flexibility than when using streamkit in OBS.

Warning
----------

This is a work in progress, not operational yet.

Install
----------

    npm install
    cp config.default.json config.json
    vi config.json
    npm start

Check https://discordhelp.net/discord-token to find out the token to use.

Use in OBS
-------------

- Tweak `index.html` to your css needs (or don't and put the css in OBS)
- Add a new browser source on http://localhost:1314

But but but ... Whyyyy?
-------------------------

- Because I'm streaming for my friends in France from Taiwan and I want to tweak the timestamp for french timezone, which is not an available option on streamkit
- Because I want more flexibility on the size of the chat display
- Because free software is always better than proprietary software.
- Because it's fun and because I can
- and I want it super lightweight, so, no discord lib, ultimately I wanna be able to tap on other chat systems.

Terms of Use
--------------

This code is copyright (c) mose and available under the terms of the MIT license.
